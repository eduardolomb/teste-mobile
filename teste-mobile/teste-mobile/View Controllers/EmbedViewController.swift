//
//  EmbedViewController.swift
//  teste-mobile
//
//  Created by Eduardo Lombardi on 10/01/18.
//  Copyright © 2018 Eduardo Lombardi. All rights reserved.
//

import UIKit

class EmbedViewController: UIViewController {




    @IBOutlet weak var uiEmbedWebview: UIWebView?
    @IBOutlet weak var uiBackButton: UIButton?
    @IBOutlet weak var uiLogo: UIImageView?
    @IBOutlet weak var uiTitle: UILabel?
    @IBOutlet weak var uiViewsLabel: UILabel?
    @IBOutlet weak var uiDescription: UILabel?
    @IBOutlet weak var uiLikesLabel: UILabel?
    @IBOutlet weak var uiDislikesLabel: UILabel?
    
    var videoObj:Video = Video(title: "", description: "", thumb: nil, id: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let id = self.videoObj.id else {
            return
        }
        
        loadVideo(id)
        self.uiTitle?.text = self.videoObj.title
        self.uiDescription?.text = self.videoObj.description
        guard let views = self.videoObj.viewCount,
              let likes = self.videoObj.likeCount,
              let dislikes = self.videoObj.dislikeCount
              else {
                return
        }
        
        self.uiViewsLabel?.text = views + " visualizações."
        self.uiLikesLabel?.text = likes + " ⬆️"
        self.uiDislikesLabel?.text = dislikes + " ⬇️"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadVideo(_ id:String) {
        let url = URL(string: "https://www.youtube.com/embed/\(id)?playsinline=1")
    
        guard let unpackedUrl = url else {
            return
        }
        let request = URLRequest(url: unpackedUrl)
        uiEmbedWebview?.loadRequest(request)
    }
    
    @IBAction func dismissBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
