//
//  ViewController.swift
//  teste-mobile
//
//  Created by Eduardo Lombardi on 10/01/18.
//  Copyright © 2018 Eduardo Lombardi. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var uiLogo: UIImageView?
    @IBOutlet weak var uiSearchField: UITextField?
    @IBOutlet weak var uiSearchButton: UIButton?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func searchBtnTap(_ sender: Any) {
    }
    
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "listViewSegue" {
            let list = search()
            let nextScene = segue.destination as? ListViewController
            guard let listView = nextScene else {
                return
            }
            guard let text = uiSearchField?.text else {
                return
            }
            
            listView.searchText = text
            listView.list = list

        }
    }

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "listViewSegue" {
            return validateTextField()
        }
        return false
    }
    
    func search() -> NSArray {
        let search = SearchObj(text: uiSearchField?.text)
        search.downloadResults()
        guard let list = search.videosList else {
            return []
        }
        return list
    }
    
    func validateTextField() -> Bool {
        if let text = uiSearchField?.text, !text.isEmpty
        {
            return true
        } else {
            let alertView = UIAlertView(title: "Erro", message: "Campo de texto vazio.", delegate: self, cancelButtonTitle: "OK")
            alertView.show()
            return false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.performSegue(withIdentifier: "listViewSegue", sender: self)
        uiSearchField?.resignFirstResponder()
        validateTextField()
        return true
    }
    

    
}
