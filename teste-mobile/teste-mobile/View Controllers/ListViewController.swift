//
//  ListViewController.swift
//  teste-mobile
//
//  Created by Eduardo Lombardi on 10/01/18.
//  Copyright © 2018 Eduardo Lombardi. All rights reserved.
//

import UIKit
import SDWebImage


class ListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate {
    

    @IBOutlet weak var uiLogo: UIImageView?
    @IBOutlet weak var uiSearchField: UITextField?
    @IBOutlet weak var uiSearchButton: UIButton?
    @IBOutlet weak var uiVideosTable: UITableView?
    
    var searchText:String = ""
    var list:NSArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.uiSearchField?.text = searchText
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.list.count * 2
        }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if (indexPath.row % 2 == 0) {
                return 105
            } else {
                return 25
            }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.row % 2 != 0) {
            return UITableViewCell()
        }
        let videoCell:VideoTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "videocell", for: indexPath) as? VideoTableViewCell

        let row = indexPath.row / 2
        guard let cell = videoCell,
                let obj = self.list.object(at: row) as? Video
                else {
                    print("Falha em descompactar a célula ou seu objeto para a linha selecionada")
                    return UITableViewCell()
        }
        cell.uiTitle?.text = obj.title
        cell.uiSubtitle?.text = obj.description
        cell.uiThumbnail?.sd_setImage(with: obj.thumb, completed: nil)
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if (indexPath.row % 2 != 0 ) {
            return nil
        }
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        guard let embedController = self.storyboard?.instantiateViewController(withIdentifier: "embedViewController") as? EmbedViewController else {
            return
        }
        guard let video = list.object(at: indexPath.row/2) as? Video else {
            return
        }
        video.downloadVideoData()
        embedController.videoObj = video
        self.present(embedController, animated: true, completion: nil)
    }
    
    @IBAction func searchBtnTap(_ sender: Any) {
        search()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        search()
        return true
    }
    
    func search() {
        if validateTextField() {
        let search = SearchObj(text: uiSearchField?.text)
        search.downloadResults()
        guard let list = search.videosList else {
            return
        }
        self.list = list
        self.uiVideosTable?.reloadData()
        }
    }
    
    func validateTextField() -> Bool {
        if let text = uiSearchField?.text, !text.isEmpty
        {
            return true
        } else {
            let alertView = UIAlertView(title: "Erro", message: "Campo de texto vazio.", delegate: self, cancelButtonTitle: "OK")
            alertView.show()
            return false
        }
    }
    
    
}
