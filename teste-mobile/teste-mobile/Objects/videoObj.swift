//
//  videoObj.swift
//  teste-mobile
//
//  Created by Eduardo Lombardi on 10/01/18.
//  Copyright © 2018 Eduardo Lombardi. All rights reserved.
//

import Foundation

class Video {
    
    let title:String?
    let description:String?
    let thumb:URL?
    let id:String?
    var viewCount:String? = ""
    var likeCount:String? = ""
    var dislikeCount:String? = ""
    
    init(title:String?,description:String?,thumb:URL?,id:String?) {
        self.title = title
        self.description = description
        self.thumb = thumb
        self.id = id
    }

    func downloadVideoData() {
        
        let key = "AIzaSyBB5LtE9HkrGHCcgbJE0JaFAA5py2NmrWk"
        
        guard let id = self.id else {
            return
        }
        let urlString = "https://www.googleapis.com/youtube/v3/videos?id=" +  id + "&part=snippet,statistics&key=" + key
        
        let url = URL(string: urlString)
        guard let unpackedUrl = url else {
            return
        }
        
        let request = URLRequest(url: unpackedUrl)
        
        do {
            let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
            let data = try NSURLConnection.sendSynchronousRequest(request, returning: response)
            let jsonSerialized = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
            
            if let json = jsonSerialized, let items = json["items"] {
                
                
                guard let item = items as? NSArray,
                      let info = item.object(at:0) as? NSDictionary,
                      let stats = info.object(forKey: "statistics") as? NSDictionary
                      else {
                        return
                }
            
                self.viewCount = stats.object(forKey: "viewCount") as? String
                self.likeCount = stats.object(forKey: "likeCount") as? String
                self.dislikeCount = stats.object(forKey: "dislikeCount") as? String
            }
            
            
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        
    }
}
