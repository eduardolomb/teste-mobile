//
//  searchObj.swift
//  teste-mobile
//
//  Created by Eduardo Lombardi on 10/01/18.
//  Copyright © 2018 Eduardo Lombardi. All rights reserved.
//

import Foundation
 final class SearchObj {
    
    var text:String?
    var videosList:NSMutableArray?
    
    init(text:String?) {
        self.text = text
        self.videosList = []
    }
    
    func downloadResults() {
        let key = "AIzaSyBB5LtE9HkrGHCcgbJE0JaFAA5py2NmrWk"
        
        guard let t = self.text else {
            return
        }
        
        let urlString = "https://www.googleapis.com/youtube/v3/search?part=id,snippet&q=" + t + "&key=" + key
        let escapedUrl = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        guard let unwrapedUrl = escapedUrl else {
            return
        }
        
        let url = URL(string: unwrapedUrl)
       
        guard let unpackedUrl = url else {
            return
        }
        
        let request = URLRequest(url: unpackedUrl)
        do {
            let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
            let data = try NSURLConnection.sendSynchronousRequest(request, returning: response)
            let jsonSerialized = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
            if let json = jsonSerialized, let items = json["items"] {

                var l:NSArray? = []
                l = items as? NSArray
                
                guard let list = l else {
                    return
                }

                for video in list {
                
                    guard let v = video as? NSDictionary,
                    let id = v.object(forKey: "id") as? NSDictionary,
                    let kind = id.object(forKey: "kind") as? String,
                    let videoId = id.object(forKey: "videoId") as? String,
                    let snippet = v.object(forKey: "snippet") as? NSDictionary,
                    let thumbnails = snippet.object(forKey: "thumbnails") as? NSDictionary,
                    let defaultThumb = thumbnails.object(forKey: "medium") as? NSDictionary,
                    let thumb = defaultThumb.object(forKey: "url") as? String
                    else{
                        print("content is not a video, skipping invalid item")
                        continue
                    }

                    
                    let title = snippet.object(forKey: "title") as? String
                    
                    let description = snippet.object(forKey: "description") as? String
                    
                    let thumbUrl = URL(string: thumb)
                    
                    let videoObj = Video(title: title,description: description,thumb: thumbUrl,id: videoId)
                    videosList?.add(videoObj)
                }
            }
        } catch let error as NSError {
                print(error.localizedDescription)
        }
    }
    
    
}
