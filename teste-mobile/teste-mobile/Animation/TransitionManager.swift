//
//  TransitionManager.swift
//  teste-mobile
//
//  Created by Eduardo Lombardi on 11/01/18.
//  Copyright © 2018 Eduardo Lombardi. All rights reserved.
//

import UIKit

class TransitionManager: UIStoryboardSegue {
    
    
    override func perform() {
       
        let sourceController = self.source as? ViewController
        
        let firstVCView = self.source.view as UIView?
        let secondVCView = self.destination.view as UIView?
        
       
        let screenWidth = UIScreen.main.bounds.size.width
        let screenHeight = UIScreen.main.bounds.size.height
        
       
        secondVCView?.frame = CGRect(x: 0.0, y: screenHeight, width: screenWidth, height: screenHeight)
        
       
        
        guard let secondView = secondVCView,
            let firstView = firstVCView else {
                return
        }
      let window = UIApplication.shared.keyWindow
        window?.insertSubview(firstView, aboveSubview: secondView)
        
        // Animate the transition.
        UIView.animate(withDuration: 0.8, animations: { () -> Void in
            sourceController?.uiSearchField?.frame.origin.y -= 205
            sourceController?.uiSearchField?.frame.size.height -= 11
            sourceController?.uiSearchField?.frame.origin.x -= 3
            
            sourceController?.uiLogo?.frame.origin.y -= 104
            sourceController?.uiLogo?.frame.size.height -= 35
            sourceController?.uiLogo?.frame.size.width -= 150
            sourceController?.uiLogo?.center.x = firstView.center.x

            sourceController?.uiSearchButton?.frame.size.height -= 11
            sourceController?.uiSearchButton?.frame.origin.y -= 270
            sourceController?.uiSearchButton?.frame.size.width -= 270
            sourceController?.uiSearchButton?.frame.origin.x += 273
        }) { (Finished) -> Void in
            self.source.present(self.destination as UIViewController,
                                                            animated: false,
                                                            completion: nil)
        }
        //super.perform()
    }

}
